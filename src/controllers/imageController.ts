import { Request, Response } from "express";

const axios = require('axios');
const crypto = require('crypto');

export class ImageController {
    public async index(req: Request, res: Response) {

        try {
            let keyword = (req.query.keyword !== undefined) ? req.query.keyword : '';

            const pixabayDatas = await pixabaySearch(String(keyword));
            const unsplashDatas = await unsplashSearch(String(keyword));
            const storyblocksDatas = await storyblocksSearch(String(keyword));

            let responseDatas = [];

            pixabayDatas.hits.map((pixabayData) => {
                responseDatas.push({
                    image_id: pixabayData.id,
                    thumbnails: pixabayData.pageURL,
                    preview: pixabayData.previewURL,
                    title: pixabayData.webformatURL,
                    source: 'Pixabay',
                    tags: pixabayData.tags
                });
            });

            unsplashDatas.results.map((unsplashData) => {
                responseDatas.push({
                    image_id: unsplashData.id,
                    thumbnails: unsplashData.urls.thumb,
                    preview: unsplashData.urls.full,
                    title: unsplashData.description,
                    source: 'Unsplash',
                    tags: []
                });
            });

            storyblocksDatas.results.map((storyblocksData) => {
                responseDatas.push({
                    image_id: storyblocksData.id,
                    thumbnails: storyblocksData.thumbnail_url,
                    preview: storyblocksData.preview_url,
                    title: storyblocksData.title,
                    source: 'Storyblocks',
                    tags: []
                });
            });

            return res.status(200).json(responseDatas).end();
        } catch (e: any) {
            return res.status(500).json({message:e.message}).end();
        }
    }
}

interface pixabayResponse {
	total: number;
	totalHits: number;
	hits: [];
}

const pixabaySearch = async (keyword: string) => {

    const pixabayApi = process.env.PIXABAY_API;
    const pixabayKey = process.env.PIXABAY_KEY;
	const options = {
		method: 'GET',
		url: pixabayApi,
		params: {
            key: pixabayKey,
            q: keyword,
        },
	};

	return await axios.request(options)
        .then(function ({ data }: { data: pixabayResponse }) {
			return data;
		})
		.catch(function (e: any) {
			throw new Error(e.message);
		});
}

interface unsplashResponse {
	total: number;
	total_pages: number;
	results: [];
}

const unsplashSearch = async (keyword: string) => {

    const unsplashApi = process.env.UNSPLASH_API;
    const unsplashKey = process.env.UNSPLASH_ACCESS_KEY;
    const options = {
		method: 'GET',
		url: unsplashApi,
		params: {
            client_id: unsplashKey,
            query:keyword
        },
		headers: {
			'Accept-Version': 'v1',
		},
	};

	return await axios.request(options)
		.then(function ({ data }: { data: unsplashResponse }) {
			return data;
		})
		.catch(function (e: any) {
			throw new Error(e.message);
		});
}

interface storyblocksResponse {
	total_results: number;
	results: [];
}

const storyblocksSearch = async (keyword: string) => {

    const expires = Math.floor(Date.now() / 1000) + 100;
    const storyBlocksApiBaseUrl = process.env.STORYBLOCKS_API_BASE_URL;
    const storyBlocksApiImageSearch = process.env.STORYBLOCKS_API_IMAGE_SEARCH;
    const storyBlocksPublicKey = process.env.STORYBLOCKS_PUBLIC_KEY;
    const hmac = await generateStoryblocksHash(storyBlocksApiImageSearch, expires);

    const options = {
		method: 'GET',
		url: storyBlocksApiBaseUrl + storyBlocksApiImageSearch,
		params: {
            APIKEY: storyBlocksPublicKey,
            EXPIRES:expires,
            HMAC: hmac,
            project_id: 'test',
            user_id: 'test',
            keywords:keyword,
        },
		headers: {
			'Content-Type': 'application/json',
		},
	};

	return await axios.request(options)
		.then(function ({ data }: { data: storyblocksResponse }) {
			return data;
		})
		.catch(function (e: any) {
			throw new Error(e.message);
		});
}

const generateStoryblocksHash = async (queryResource : string, expires : number) => {
    var privateKey = process.env.STORYBLOCKS_PRIVATE_KEY;
    const hmacBuilder = crypto.createHmac('sha256', privateKey + expires);
    hmacBuilder.update(queryResource);
    return hmacBuilder.digest('hex');    
}

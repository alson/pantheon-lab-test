import express, { Request, Response, NextFunction } from 'express';
import compression = require("compression");
import cors = require("cors");
import * as dotenv from "dotenv";

// Routes
import { ImageRoutes } from "./routes/imageRoutes";

class Server {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.dotenv();
        this.config();
        this.routes();
    }

    public dotenv(): void {
        let path = `${__dirname}/../../.env`;
        dotenv.config();
        dotenv.config({ path: path });
    }

    public config(): void {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(compression());
        this.app.use(cors());
        this.app.use(function(req, res, next){
            res.setTimeout(1800000, function(){
                console.log('Request has timed out.');
                    res.send(408);
                });
      
            next();
        });
    }

    public routes(): void {
        this.app.use("/search-image", new ImageRoutes().router);
    }

    public start(): void {
        this.app.listen(this.app.get("port"), () => {
            console.log("API is running at http://localhost:%s", this.app.get("port"));
        });
    }
}

const server = new Server();

server.start();

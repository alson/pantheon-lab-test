import { Router } from "express";
import { ImageController } from "../controllers/imageController";


export class ImageRoutes {

    router: Router;
    public imageController: ImageController = new ImageController();

    constructor() {
        this.router = Router();
        this.routes();
    }
    routes() {

        this.router.all('*', (req, res, next)=>{
            req.body.user_type = 'Any';
            return next();
        });

        this.router.get("/", (req, res) => this.imageController.index(req, res));
    }
}
# How it works
- The API dispatches requests with well structured **routes**.
- Routes are using **controllers** for API implementations.

# How to run
- use node version 16.13.1 to run this program
- create .env file based on the sample of .env.example
- type `npm install` to install node modules
- type `npm run serve` to start the program
- example endpoint = http://localhost:3000/search-image?keyword=mushrooms